
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Tes1NormalRam IS
END Tes1NormalRam;
 
ARCHITECTURE behavior OF Tes1NormalRam IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MemoriaPrograma
    PORT(
         dir : IN  std_logic_vector(11 downto 0);
         dato : OUT  std_logic_vector(24 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal dato : std_logic_vector(24 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MemoriaPrograma PORT MAP (
          dir => dir,
          dato => dato
        );

   -- Stimulus process
   stim_proc: process
   begin		
      dir<="000000000000";
		wait for 100 ns;
		dir<="000000000001";
		wait for 100 ns;
		dir<="000000000010";
		wait for 100 ns;
		dir<="000000000101";
		wait for 100 ns;
		dir<="000000000110";
		wait for 100 ns;
		dir<="000000000011";
		wait for 100 ns;
		dir<="000000100100";
		wait for 100 ns;
		dir<="000000000010";
		wait for 100 ns;
		dir<="000000000101";
		wait for 100 ns;
			
      wait;
   end process;

END;
