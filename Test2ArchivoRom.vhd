LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	--PERMITE USAR STD_LOGIC 

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;

ENTITY Test2ArchivoRom IS
END Test2ArchivoRom;
 
ARCHITECTURE behavior OF Test2ArchivoRom IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MemoriaPrograma
    PORT(
         dir : IN  std_logic_vector(11 downto 0);
         dato : OUT  std_logic_vector(24 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal dato : std_logic_vector(24 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MemoriaPrograma PORT MAP (
          dir => dir,
          dato => dato
        );


   -- Stimulus process
	   stim_proc: process
	
	--variables de archivo
	
	file ARCH_RES :  TEXT;	--archivo de salida
	file ARCH_ENT : TEXT; --archivo de entrada	
	
	variable LINEA_RES : line;
	variable LINEA_ENT : line;
	
	-- variables de entrada
	
	variable var_dir : std_logic_vector(11 downto 0) := (others => '0');



 	--varibales de salida
   variable var_dato : std_logic_vector(24 downto 0);

	
   begin		
	
		file_open(ARCH_ENT, "ENTRADA.txt", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.txt", WRITE_MODE); 
		
		WHILE NOT ENDFILE(ARCH_ENT) LOOP
			readline(ARCH_ENT,LINEA_ENT); 

			read(LINEA_ENT, var_dir);
			dir <= var_dir;
			
			wait for 100 ns;
			
			var_dato := dato;
			
			write(LINEA_RES, var_dato, right, 5);	--ESCRIBE EL CAMPO 

			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
		end loop;
		file_close(ARCH_ENT);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo
		
   end process;

END;
