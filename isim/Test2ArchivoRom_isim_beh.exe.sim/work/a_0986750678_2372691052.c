/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/practica6/Test2ArchivoRom.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;

void ieee_p_3564397177_sub_1281154728_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_2889341154_91900896(char *, char *, char *, char *, char *);


static void work_a_0986750678_2372691052_p_0(char *t0)
{
    char t5[16];
    char t14[32];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    int64 t13;

LAB0:    t1 = (t0 + 2944U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(67, ng0);
    t2 = (t0 + 2040U);
    t3 = (t0 + 5513);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 11;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (11 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(68, ng0);
    t2 = (t0 + 1936U);
    t3 = (t0 + 5524);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 13;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (13 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(70, ng0);

LAB4:    t2 = (t0 + 2040U);
    t10 = std_textio_endfile(t2);
    t11 = (!(t10));
    if (t11 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(85, ng0);
    t2 = (t0 + 2040U);
    std_textio_file_close(t2);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 1936U);
    std_textio_file_close(t2);
    goto LAB2;

LAB5:    xsi_set_current_line(71, ng0);
    t3 = (t0 + 2752);
    t4 = (t0 + 2040U);
    t6 = (t0 + 2288U);
    std_textio_readline(STD_TEXTIO, t3, t4, t6);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 2752);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t4 = (t0 + 5252U);
    ieee_p_3564397177_sub_2889341154_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 1488U);
    t3 = *((char **)t2);
    t2 = (t0 + 3328);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t12 = *((char **)t7);
    memcpy(t12, t3, 12U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(76, ng0);
    t13 = (100 * 1000LL);
    t2 = (t0 + 2752);
    xsi_process_wait(t2, t13);

LAB10:    *((char **)t1) = &&LAB11;

LAB1:    return;
LAB6:;
LAB8:    xsi_set_current_line(78, ng0);
    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t2 = (t0 + 1608U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 25U);
    xsi_set_current_line(80, ng0);
    t2 = (t0 + 2752);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    memcpy(t14, t6, 25U);
    t4 = (t0 + 5268U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t14, t4, (unsigned char)0, 5);
    xsi_set_current_line(82, ng0);
    t2 = (t0 + 2752);
    t3 = (t0 + 1936U);
    t4 = (t0 + 2216U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB4;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}


extern void work_a_0986750678_2372691052_init()
{
	static char *pe[] = {(void *)work_a_0986750678_2372691052_p_0};
	xsi_register_didat("work_a_0986750678_2372691052", "isim/Test2ArchivoRom_isim_beh.exe.sim/work/a_0986750678_2372691052.didat");
	xsi_register_executes(pe);
}
