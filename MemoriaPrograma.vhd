library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity MemoriaPrograma is
generic (
		tam_dir : integer := 12;
		tam_dat : integer := 25
	);
   Port ( dir : in  STD_LOGIC_VECTOR (tam_dir-1 downto 0);
          dato : out  STD_LOGIC_VECTOR (tam_dat-1 downto 0));
end MemoriaPrograma;

architecture Behavioral of MemoriaPrograma is
constant R1 : std_logic_vector(3 downto 0) := "0001";
constant R0 : std_logic_vector(3 downto 0) := "0000";
constant OP_LI : std_logic_vector(4 downto 0) := "00001";
constant OP_CALL : std_logic_vector(4 downto 0) := "10100";
constant OP_SWI : std_logic_vector(4 downto 0) := "00011";
constant OP_B : std_logic_vector(4 downto 0) := "10011";
constant OP_TR : std_logic_vector(4 downto 0) := "00000";
constant OP_RET : std_logic_vector(4 downto 0) := "10101";
constant S_U: std_logic_vector(3 downto 0) :="0000";

type memoria_rom is array (0 to ((2**tam_dir)-1)) of std_logic_vector (tam_dat-1 downto 0);
constant rom : memoria_rom := (
						OP_LI & R0 & x"0001", -- LI R0,#1 
						OP_LI & R0 & x"0007", --LI R0,#7 
						OP_CALL & S_U & x"0005", --CALL
						OP_SWI & R1 & x"0005", --SWI R1,5 
						OP_B & S_U & x"0002",
						OP_TR & R1 & R1 & R0 & S_U & "0000",
						OP_RET & S_u & S_u & S_u & S_u & S_u,
						others => (others => '0'));

begin

	dato <= rom (conv_integer(dir));
end Behavioral;


